# Utsukumi

A backend library to create and manage wallpaper states.

## Naming
The name is taken from the Kun'yomi reading of "美観", which translates to "fine
view / beautiful sight". The proper reading of the word uses the On'yomi reading
which is "びかん", but I liked the sound of the Kun'yomi reading more.

## License
Utsukumi is free software, distributed under the terms of the
General Public License as published by the Free Software Foundation,
version 3 (or later). For more information, see the COPYING file.
