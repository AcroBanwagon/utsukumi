use clap::CommandFactory;
use clap_complete::generate_to;
use clap_complete::shells::Shell;
use std::env;
use std::io::Error;

include!("src/cli.rs");

const SHELLS: [Shell; 3] = [Shell::Bash, Shell::Zsh, Shell::Fish];

fn main() -> Result<(), Error> {
    let outdir = match env::var_os("OUT_DIR") {
        None => return Ok(()),
        Some(outdir) => outdir,
    };

    let mut cmd = Cli::command();

    for shell in SHELLS {
        let path = generate_to(shell, &mut cmd, "utsukumi", &outdir)?;
        println!("cargo:warning=completion file is generated: {path:?}");
    }

    Ok(())
}
