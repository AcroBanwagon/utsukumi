use clap::Parser;
use std::path::PathBuf;

/// Generate and manage wallpapers.
#[derive(Parser, Debug)]
pub struct Cli {
    /// Specify output name to override. Overridden args are placed after this argument.
    /// A single "-o Global" is always implied at the beginning of the cmdline
    #[arg(short = 'o', long = "output", default_value = "Global")]
    pub output_name: Option<String>,

    // Must be Option so that outputs can fallback to input specified for the global space.
    /// Path to image to use as a wallpaper or video thumbnail to use as lockscreen.
    #[arg(short)]
    pub input_path: Option<PathBuf>,

    /// Rotate the input image
    #[arg(short, value_parser = clap::value_parser!(u16).range(0..=360))]
    pub rotation_degrees: Option<u16>,

    /// Crop and spanned horizonal offset.
    #[arg(
        short = 'x',
        long = "offset-x",
        default_value = "0",
        allow_hyphen_values = true
    )]
    pub offset_x: Option<i32>,

    /// Crop and spanned vertial offset.
    #[arg(
        short = 'y',
        long = "offset-y",
        default_value = "0",
        allow_hyphen_values = true
    )]
    pub offset_y: Option<i32>,

    // Pass a geometry arg, aspect ratio or W x H.
    // Parse it like imagemagick.
    /// Crop geometry, either a resolution or aspect ratio.
    /// Ex: 1920x1080 or 16:9
    #[arg(short, long)]
    pub crop: Option<String>,

    // Doesn't make sense as a per-output arg, but afaik there isn't really a clean way for clap to
    // handle the difference between global output space and per-output.
    /// Span the input across all non-overridden outputs
    #[arg(short, long)]
    pub spanned: bool,

    /// Path to video to use as wallpaper. Format must be supported by the setter.
    #[arg(long = "video")]
    pub video: Option<PathBuf>,

    /// Quickly change to a pre-defined wallpaper.
    #[arg(long = "emergency")]
    pub is_emergency: bool,

    /// Restore the current wallpaper on login.
    #[arg(long = "login")]
    pub is_login: bool,

    /// Lock the session with the current wallpaper.
    #[arg(long = "lockscreen")]
    pub is_lockscreen: bool,

    /// View cached state in a human readable format.
    #[arg(long = "dump-cached")]
    pub dump_cached: Option<PathBuf>,
}
