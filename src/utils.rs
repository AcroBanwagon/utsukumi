use crate::consts::*;
use crate::output::Output;
use crate::wallpaper::Wallpaper;

use image::imageops::{blur, crop_imm};
use image::io::Reader;
use image::{DynamicImage, GenericImageView, ImageBuffer, Pixel, Primitive};
use imageproc::geometric_transformations::{rotate_about_center, Interpolation};
use mlua::{Error, Result, Table};
use num_traits::{clamp, NumCast};
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::f32::consts::PI;
use std::fs::File;
use std::io::{copy, BufReader};
use std::path::{Path, PathBuf};

fn rotate_image_degrees(image: DynamicImage, degrees: u16) -> DynamicImage {
    if degrees == 90 {
        image.rotate90()
    } else if degrees == 180 {
        image.rotate180()
    } else if degrees == 270 {
        image.rotate270()
    } else if degrees != 0 {
        let rotation_radians = degrees as f32 * (PI / 180.0);
        // Instead of just filling in with white like magick, take a pixel from the
        // image so that the colors match.
        let fill_pixel = image.get_pixel(0, 0);
        image::DynamicImage::ImageRgba8(rotate_about_center(
            &image.to_rgba8(),
            rotation_radians,
            Interpolation::Bilinear,
            fill_pixel,
        ))
    } else {
        image
    }
}

fn crop_from_arg(image: &DynamicImage, crop: &str, offset: &(u32, u32)) -> Result<DynamicImage> {
    // The crop_parameter is either going to be an aspect ratio or an exact resolution.
    // So it's two numbers separated by either a ':' or 'x'.
    let source_width = image.width();
    let source_height = image.height();
    let source_ratio = source_width as f32 / source_height as f32;

    // Attempt to split for both cases and take advantage of Option to decide which case it is.
    let target_aspect_ratio = crop.split_once(':');
    let target_resolution = crop.split_once('x');

    let target_width: u32;
    let target_height: u32;

    if let Some(target_aspect) = target_aspect_ratio {
        let aspect_width: f32 = target_aspect.0.parse().map_err(Error::external)?;
        let aspect_height: f32 = target_aspect.1.parse().map_err(Error::external)?;
        let target_ratio: f32 = aspect_width / aspect_height;

        match target_ratio.partial_cmp(&source_ratio) {
            Some(Ordering::Greater) => {
                target_width = source_width;
                target_height = (source_width as f32 * target_ratio.recip()).round() as u32;
            }
            Some(Ordering::Less) => {
                target_width = (source_height as f32 * target_ratio).round() as u32;
                target_height = source_height;
            }
            Some(Ordering::Equal) => {
                target_width = source_width;
                target_height = source_height;
            }
            None => {
                return Err(Error::external(
                    "could not compare target_ratio to source_ratio!",
                ))
            }
        }
    } else if let Some(resolution) = target_resolution {
        target_width = resolution.0.parse().map_err(Error::external)?;
        target_height = resolution.1.parse().map_err(Error::external)?;
    } else {
        return Err(Error::external("invalid crop parameter!"));
    }

    if offset.0 + target_width > source_width || offset.1 + target_height > source_height {
        return Err(Error::external("offset pushes crop outside of image!"));
    }

    Ok(DynamicImage::ImageRgba8(
        crop_imm(image, offset.0, offset.1, target_width, target_height).to_image(),
    ))
}

fn load_image<T: AsRef<Path>>(path: T) -> Result<DynamicImage> {
    let image_file = File::open(path)?;
    let mut image_reader = Reader::new(BufReader::new(image_file));
    image_reader.no_limits();
    image_reader
        .with_guessed_format()?
        .decode()
        .map_err(Error::external)
}

fn load_image_rotated<T: AsRef<Path>>(path: T, degrees: u16) -> Result<DynamicImage> {
    Ok(rotate_image_degrees(load_image(path)?, degrees))
}

pub fn setup_simple<'lua>(
    wallpaper: &Wallpaper,
    filename_prefix: &str,
    env_table: &Table,
    lua: &'lua mlua::Lua,
) -> Result<Table<'lua>> {
    let locations = lua.create_table()?;
    let state_dir: String = env_table.get(ENV_TABLE_STATE_DIR_KEY)?;
    let cache_dir: String = env_table.get(ENV_TABLE_CACHE_DIR_KEY)?;

    let mut wallpaper_path = PathBuf::new();
    wallpaper_path.push(&state_dir);
    wallpaper_path.push(filename_prefix);
    wallpaper_path.set_extension("png");

    let mut lockscreen_path = PathBuf::new();
    lockscreen_path.push(&state_dir);
    lockscreen_path.push(format!("{filename_prefix}-lockscreen"));
    lockscreen_path.set_extension("png");

    let mut wallpaper_cache_path = PathBuf::new();
    wallpaper_cache_path.push(&cache_dir);
    wallpaper_cache_path.push(wallpaper.base64_hash());
    wallpaper_cache_path.set_extension("png");

    let mut lockscreen_cache_path = PathBuf::new();
    lockscreen_cache_path.push(&cache_dir);
    lockscreen_cache_path.push(format!("{}-lockscreen", wallpaper.base64_hash()));
    lockscreen_cache_path.set_extension("png");

    if wallpaper_cache_path.is_file() {
        println!(
            "Found cache for: {} {filename_prefix}",
            wallpaper.source_path().display()
        );

        let mut cached_wallpaper: File = File::open(&wallpaper_cache_path)?;
        let mut cached_lockscreen: File = File::open(&lockscreen_cache_path)?;

        let mut state_wallpaper = File::create(&wallpaper_path)?;
        let mut state_lockscreen = File::create(&lockscreen_path)?;

        let _wall_bytes = copy(&mut cached_wallpaper, &mut state_wallpaper)?;
        let _lock_bytes = copy(&mut cached_lockscreen, &mut state_lockscreen)?;
    } else {
        let mut source_image =
            load_image_rotated(wallpaper.source_path(), *wallpaper.rotation_degrees())?;

        let offset_cmdline = wallpaper.offset();

        if offset_cmdline.0.is_negative() || offset_cmdline.1.is_negative() {
            Err(Error::external("offset can't be negative for simple fill!"))?;
        }

        let offset = (offset_cmdline.0 as u32, offset_cmdline.1 as u32);

        if !wallpaper.source_crop().is_empty() {
            source_image = crop_from_arg(&source_image, wallpaper.source_crop(), &offset)?;
        }

        let lockscreeen = create_lockscreen(&source_image);

        source_image
            .save(&wallpaper_path)
            .map_err(Error::external)?;
        source_image
            .save(&wallpaper_cache_path)
            .map_err(Error::external)?;

        lockscreeen
            .save(&lockscreen_path)
            .map_err(Error::external)?;
        lockscreeen
            .save(&lockscreen_cache_path)
            .map_err(Error::external)?;
    }

    // Manipulating the video thumbnail might be desired although niche, so any modifiers should
    // still be applied to the thumbnail.
    if let Some(video_path) = wallpaper.video_path() {
        locations.set(LOCATION_TABLE_VIDEO_KEY, video_path.to_string_lossy())?;
    }

    locations.set(
        LOCATION_TABLE_WALLPAPER_KEY,
        wallpaper_path.to_string_lossy(),
    )?;
    locations.set(
        LOCATION_TABLE_LOCKSCREEN_KEY,
        lockscreen_path.to_string_lossy(),
    )?;

    locations.set(
        LOCATION_TABLE_WALLPAPER_CACHE_KEY,
        wallpaper_cache_path.to_string_lossy(),
    )?;
    locations.set(
        LOCATION_TABLE_LOCKSCREEN_CACHE_KEY,
        lockscreen_cache_path.to_string_lossy(),
    )?;

    Ok(locations)
}

pub fn setup_spanned<'lua>(
    wallpaper: &mut Wallpaper,
    outputs: &[Output],
    env_table: &Table,
    lua: &'lua mlua::Lua,
) -> Result<HashMap<String, Table<'lua>>> {
    let mut name_and_location: HashMap<String, Table> = HashMap::new();
    let mut source_image =
        load_image_rotated(wallpaper.source_path(), *wallpaper.rotation_degrees())?;

    // Get dimensions of the spannable output space. The width would either be the
    // largest value of the x position (which would be the furthest output) + the
    // furthest output's width or just the width of the largest output if it's
    // larger than the previous calculation. The same applies for the height.
    let mut positions_x: BTreeMap<u32, &Output> = BTreeMap::new();
    let mut positions_y: BTreeMap<u32, &Output> = BTreeMap::new();
    let mut widths: BTreeSet<u32> = BTreeSet::new();
    let mut heights: BTreeSet<u32> = BTreeSet::new();
    for output in outputs {
        positions_x.insert(output.position().0, output);
        positions_y.insert(output.position().1, output);
        widths.insert(output.resolution().0);
        heights.insert(output.resolution().1);
    }

    // Should just be able to unwrap since this should only run if
    // spannable_outputs actually has anything in it.
    let largest_x = positions_x
        .last_key_value()
        .expect("spannable_outputs should have non-empty Outputs!");
    let largest_y = positions_y.last_key_value().unwrap();
    let largest_width = widths.last().unwrap();
    let largest_height = heights.last().unwrap();

    let largest_x_and_width = largest_x.0 + largest_x.1.resolution().0;
    let largest_y_and_height = largest_y.0 + largest_y.1.resolution().1;

    let spannable_width = if largest_x_and_width > *largest_width {
        largest_x_and_width
    } else {
        *largest_width
    };

    let spannable_height = if largest_y_and_height > *largest_height {
        largest_y_and_height
    } else {
        *largest_height
    };

    // Map the outputs onto the source image by normalizing the values in relation
    // to the source image size. Then crop the mapped rectangle and save it as the
    // final wallpaper. The goal is for the output space to take up as much space
    // as possible in the image without going beyond its borders (at least without
    // a user supplied offset).
    //
    // The values can be checked with something like Inkscape, make a canvas the
    // size of the image and rectangles the size of the outputs. Position the
    // rectangles the same way that the outputs are placed relative to each other.
    // Move all the rectangles as a group and position their top left to the top
    // left of the canvas and scale them until the first canvas edge is hit. The
    // target width/height for each output should match the width/height of their
    // counterpart in Inkscape. The same applies for the offset which match their
    // x/y positions in Inkscape.

    // If the aspect ratio of the spannable output space is larger than the
    // aspect ratio of the source image, then when the output space gets mapped
    // over it, the width of the image bounds the mapped output space. i.e.,
    // the right side of the image is going to be closer to the right edge of
    // the mapped output space than the bottom of the image is to the bottom
    // of the mapped output space.
    //
    // If the spannable aspect ratio is smaller, then the opposite would be
    // true, and their respective heights should be used in place of their
    // widths.
    let mut spannable_reference_resolution = spannable_width;
    let mut source_reference_resolution = source_image.width();

    let spannable_aspect_ratio = spannable_width as f32 / spannable_height as f32;
    let source_image_aspect_ratio = source_image.width() as f32 / source_image.height() as f32;

    if let Some(Ordering::Less) = spannable_aspect_ratio.partial_cmp(&source_image_aspect_ratio) {
        spannable_reference_resolution = spannable_height;
        source_reference_resolution = source_image.height();
    }

    for output in outputs {
        let mut target_width = (output.resolution().0 as f32
            / spannable_reference_resolution as f32)
            * source_reference_resolution as f32;

        // The reciprocal of the aspect ratio * target_width can be used to find
        // the corresponding target height.
        let mut target_height =
            (output.resolution().1 as f32 / output.resolution().0 as f32) * target_width;

        // If the width of the source image is larger than its height, then the
        // aspect ratio between the spannable_width and source image width would be
        // used to find the corresponding target offsets when multiplied by the
        // outputs respective offset.
        // (output offset / target offset) = (spannable width / image width)
        let mut target_offset_x = (output.position().0 * source_reference_resolution) as f32
            / spannable_reference_resolution as f32
            + wallpaper.offset().0 as f32;

        let mut target_offset_y = (output.position().1 * source_reference_resolution) as f32
            / spannable_reference_resolution as f32
            + wallpaper.offset().1 as f32;

        // If the offset is actually negative (should only happen from user
        // supplied offset) then the respective width/height should be
        // reduced to compensate.
        if target_offset_x.is_sign_negative() {
            target_width -= target_offset_x.abs();
            target_offset_x = 0.0;
        }

        if target_offset_y.is_sign_negative() {
            target_height -= target_offset_y.abs();
            target_offset_y = 0.0;
        }

        // A user specified offset can push the region to be cropped beyond the
        // bounds of the source image. Attempting to crop that would result in at
        // least one of the sides of the wallpaper having a size of zero and then
        // returning an error when attempting to write the image to disk.
        //
        // In that case, the width/height should just be set to whatever is
        // actually available, which basically just chops off anything outside the
        // source image. When the region is completely outside the source image,
        // just return a single blank pixel as an image.
        let available_width: u32 =
            (source_image.width() as f32 - target_offset_x).clamp(0.0, f32::MAX) as u32;
        let available_height: u32 =
            (source_image.height() as f32 - target_offset_y).clamp(0.0, f32::MAX) as u32;

        if available_width == 0 {
            source_image =
                DynamicImage::ImageRgb8(ImageBuffer::from_fn(1, 1, |_, _| image::Rgb([0, 0, 0])));
            target_width = 1.0;
            target_height = 1.0;
            target_offset_x = 0.0;
            target_offset_y = 0.0;
        } else if available_width < target_width.round() as u32 {
            target_width = available_width as f32;
        }

        if available_height == 0 {
            source_image =
                DynamicImage::ImageRgb8(ImageBuffer::from_fn(1, 1, |_, _| image::Rgb([0, 0, 0])));
            target_width = 1.0;
            target_height = 1.0;
            target_offset_x = 0.0;
            target_offset_y = 0.0;
        } else if available_height < target_height.round() as u32 {
            target_height = available_height as f32;
        }

        let wallpaper_image = crop_imm(
            &source_image,
            target_offset_x.round() as u32,
            target_offset_y.round() as u32,
            target_width.round() as u32,
            target_height.round() as u32,
        )
        .to_image();

        let lockscreen_image = create_lockscreen(&wallpaper_image);

        let state_dir: String = env_table.get(ENV_TABLE_STATE_DIR_KEY)?;
        let cache_dir: String = env_table.get(ENV_TABLE_CACHE_DIR_KEY)?;

        let mut wallpaper_path = PathBuf::new();
        wallpaper_path.push(&state_dir);
        wallpaper_path.push(output.name());
        wallpaper_path.set_extension("png");

        let mut wallpaper_cache_path = PathBuf::new();
        wallpaper_cache_path.push(&cache_dir);
        wallpaper_cache_path.push(wallpaper.base64_hash());
        wallpaper_cache_path.set_extension("png");

        wallpaper_image
            .save(&wallpaper_path)
            .map_err(Error::external)?;

        wallpaper_image
            .save(&wallpaper_cache_path)
            .map_err(Error::external)?;

        let mut lockscreen_path = PathBuf::new();
        lockscreen_path.push(state_dir);
        lockscreen_path.push(format!("{}-lockscreen", output.name()));
        lockscreen_path.set_extension("png");

        let mut lockscreen_cache_path = PathBuf::new();
        lockscreen_cache_path.push(&cache_dir);
        lockscreen_cache_path.push(format!("{}-lockscreen", wallpaper.base64_hash()));
        lockscreen_cache_path.set_extension("png");

        lockscreen_image
            .save(&lockscreen_path)
            .map_err(Error::external)?;

        lockscreen_image
            .save(&lockscreen_cache_path)
            .map_err(Error::external)?;

        let paths_table = lua.create_table()?;
        paths_table.set(
            LOCATION_TABLE_WALLPAPER_KEY,
            wallpaper_path.to_string_lossy(),
        )?;
        paths_table.set(
            LOCATION_TABLE_LOCKSCREEN_KEY,
            lockscreen_path.to_string_lossy(),
        )?;

        name_and_location.insert(String::from(output.name()), paths_table);

        wallpaper.add_state_keyedpath(wallpaper_path, String::from(output.name()));
        wallpaper.add_cache_keyedpath(wallpaper_cache_path, String::from(output.name()));
        wallpaper.add_lockscreen_state_keyedpath(lockscreen_path, String::from(output.name()));
        wallpaper
            .add_lockscreen_cache_keyedpath(lockscreen_cache_path, String::from(output.name()));
    }

    Ok(name_and_location)
}

fn create_lockscreen<T, P, S>(wallpaper_image: &T) -> ImageBuffer<P, Vec<S>>
where
    T: GenericImageView<Pixel = P>,
    P: Pixel<Subpixel = S> + 'static,
    S: Primitive + 'static,
{
    let (width, height) = wallpaper_image.dimensions();
    let darkened_image = {
        let mut out = ImageBuffer::new(width, height);
        for (x, y, pixel) in wallpaper_image.pixels() {
            let darkened_pixel = pixel.map_with_alpha(
                |b| {
                    let c: f32 = NumCast::from(b).unwrap();
                    let d = clamp(c * 0.5, 0.0, f32::MAX);

                    NumCast::from(d).unwrap()
                },
                |alpha| alpha,
            );
            out.put_pixel(x, y, darkened_pixel);
        }
        out
    };

    // Ensure that all images get the same relative blur.
    // The 50.0 and 0.6 values come from wallpaper.sh, which in turn got it from betterlockscreen.
    // Changing those values would change the relative blur.
    // One note is that large sigma values take a while to blur unfortunately, which means that
    // large images take longer to process.
    let normalized_width = width as f32 / 50.0;
    let sigma = normalized_width * 0.6;
    blur(&darkened_image, sigma)
}
