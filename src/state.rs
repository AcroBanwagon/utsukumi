use crate::cli::Cli;
use crate::consts::*;
use crate::output::Output;
use crate::utils::{setup_simple, setup_spanned};
use crate::wallpaper::{KeyedPath, Wallpaper, WallpaperBuilder, WallpaperMode};

use base64ct::{Base64Url, Encoding};
use blake2::{Blake2b512, Digest};
use core::ops::Deref;
use mlua::{Error, Result, Table, UserData};
use postcard::{from_bytes, to_allocvec};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::io::{BufRead, Read, Write};
use std::path::{Path, PathBuf};
use std::fmt;
use std::{fs::remove_file, fs::File, io::BufReader};

// A struct to manage the state for the entire output space, including their wallpapers.
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct StateManager {
    global: Wallpaper,
    overrides: HashMap<Output, Wallpaper>,
}

impl StateManager {
    pub fn new() -> Self {
        StateManager {
            global: Wallpaper::new(),
            overrides: HashMap::new(),
        }
    }

    pub fn from_args(args: Vec<Cli>, outputs: &Table) -> Result<Self> {
        let mut manager = StateManager::new();
        let mut is_global = false;

        for wallpaper_args in args {
            let mut wallpaper_builder = WallpaperBuilder::new();

            if let Some(input_path) = &wallpaper_args.input_path {
                wallpaper_builder.source_path(input_path)?;
            }

            // Check for args that are meant to apply to the global space.
            // This includes any args specified before a `-o` arg.
            if let Some(name) = &wallpaper_args.output_name {
                // https://rust-lang.github.io/rust-clippy/master/index.html#/needless_bool_assign
                // That's neat.
                is_global = name == "Global";
            }

            let offset_x = wallpaper_args.offset_x.unwrap_or(0);
            let offset_y = wallpaper_args.offset_y.unwrap_or(0);

            wallpaper_builder.offset((offset_x, offset_y));

            if let Some(crop) = wallpaper_args.crop {
                wallpaper_builder.source_crop(crop);
            }

            if wallpaper_args.spanned {
                wallpaper_builder.mode(WallpaperMode::Spanned);
            }

            if let Some(rotation_degrees) = wallpaper_args.rotation_degrees {
                wallpaper_builder.rotation_degrees(rotation_degrees);
            }

            if let Some(video_path) = wallpaper_args.video {
                wallpaper_builder.video_path(video_path)?;
            }

            if is_global {
                manager.global = wallpaper_builder.build();
            } else {
                // Get output parameters directly from lua via a table. Previously, I thought it
                // would be a good idea to have a struct that already built the outputs and then
                // this function would simply take ownership of them. But since this is the only
                // place such a struct would even be needed, the table might as well be passed here
                // instead of doing anything with another UserData struct or global statics.
                let output_params: Table = outputs.get(
                    wallpaper_args
                        .output_name
                        // mlua doesn't like &str.
                        .clone()
                        .ok_or_else(|| Error::external("Could not get output name!"))?,
                )?;
                output_params.set(
                    "name",
                    wallpaper_args
                        .output_name
                        .ok_or_else(|| Error::external("Could not get output name!"))?,
                )?;
                let output = Output::from_table(&output_params)?;
                manager.overrides.insert(output, wallpaper_builder.build());
            }
        }
        Ok(manager)
    }

    pub fn from_cache<T: AsRef<Path>>(cached_path: T) -> Result<Self> {
        let mut contents: Vec<u8> = Vec::new();
        let mut cached_file = File::open(cached_path)?;
        cached_file.read_to_end(&mut contents)?;

        from_bytes(contents.deref()).map_err(Error::external)
    }

    pub fn cleanup_state(&self) -> usize {
        let mut removed = 0;

        let to_remove: HashSet<&PathBuf> = self
            .overrides
            .values()
            .flat_map(|o| o.paths())
            .chain(self.overrides.values().flat_map(|o| o.lockscreen_paths()))
            .chain(self.global.paths())
            .chain(self.global.lockscreen_paths())
            .filter_map(|keyed_path| {
                if let KeyedPath::State(_, path) = keyed_path {
                    Some(path)
                } else {
                    None
                }
            })
            .collect();

        for path in to_remove {
            let res = remove_file(path);
            if res.is_ok() {
                removed += 1;
            }
        }

        removed
    }

    pub fn update_override_fallbacks(&mut self) -> Result<usize> {
        let mut entries_changed = 0;
        for wallpaper in self.overrides.values_mut() {
            if !wallpaper.source_path().try_exists()? {
                wallpaper.set_source_path(self.global.source_path());
                entries_changed += 1;
            }
        }
        Ok(entries_changed)
    }

    pub fn hash(&self) -> [u8; 64] {
        let mut hasher = Blake2b512::new();

        hasher.update(self.global.hash());

        for wallpaper in self.overrides.values() {
            hasher.update(wallpaper.hash())
        }

        hasher.finalize().into()
    }

    pub fn base64_hash(&self) -> String {
        Base64Url::encode_string(&self.hash())
    }

    pub fn is_cached_at_path<T: AsRef<Path>>(&self, cache_path: T) -> Result<bool> {
        // Would be nice to have more descriptive errors for all the possible io errors that could
        // occur while the program runs.
        let mut is_cached = false;
        let mut index_path = PathBuf::new();
        index_path.push(cache_path);
        index_path.push(CACHED_STATEMANAGER_INDEX_FILENAME);

        if index_path.is_file() {
            let index = File::open(index_path)?;

            let index_reader = BufReader::new(index);
            let current_hash = self.base64_hash();

            for line in index_reader.lines() {
                let cached_hash = line?;
                if cached_hash == current_hash {
                    is_cached = true;
                }
            }
        }
        Ok(is_cached)
    }
}

impl fmt::Display for StateManager {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output_string = format!("==== Global ====\n{}\n", self.global);

        if self.overrides.is_empty() {
            output_string.push_str("No overrides.");
        } else {
            output_string.push_str("==== Overrides ====\n");
            for (output, wallpaper) in &self.overrides {
                output_string.push_str(&format!("Override for {}\n{}", output.name(), wallpaper))
            }
        }

        write!(f, "{}", output_string)
    }
}

impl Default for StateManager {
    fn default() -> Self {
        Self::new()
    }
}

impl UserData for StateManager {
    fn add_methods<'lua, M: mlua::UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method_mut("build_state", |lua, this, env_table: Table| {
            // Should hold an output name and the path to their wallpaper.
            let exports = lua.create_table()?;
            let output_table: Table = env_table.get(ENV_TABLE_OUTPUTS_KEY)?;
            let mut num_overridden = 0;
            let mut available_outputs: Vec<Output> = Vec::new();
            let mut unoverridden_outputs: Vec<Output> = Vec::new();

            let state_dir: String = env_table.get(ENV_TABLE_STATE_DIR_KEY)?;
            let cache_dir: String = env_table.get(ENV_TABLE_CACHE_DIR_KEY)?;
            // Each output acts as an override of global. Therefore, spanned only makes sense
            // coming from global, as each Wallpaper is really only supposed to apply for one
            // Output. The intended usecase is esentially to be able to do everything with global,
            // but be able to use per-output overrides in special cases. Such as using a different
            // crop or being excluded from a spanned wallpaper.
            for pair in output_table.pairs() {
                let (output_name, output_params): (String, Table) = pair?;
                let mut is_overridden = false;

                for output_override in this.overrides.keys() {
                    if output_name == output_override.name() {
                        is_overridden = true;
                        num_overridden += 1;
                    }
                }

                output_params.set("name", output_name)?;
                available_outputs.push(Output::from_table(&output_params)?);

                if !is_overridden {
                    unoverridden_outputs.push(Output::from_table(&output_params)?);
                }
            }

            // Cleanup previous state to avoid state_dir having unneeded files.
            let mut previous_state_path = PathBuf::new();
            previous_state_path.push(&state_dir);
            previous_state_path.push(CURRENT_STATEMANAGER_FILENAME);
            let previous_state = StateManager::from_cache(previous_state_path);

            if let Ok(manager) = previous_state {
                manager.cleanup_state();
            } else {
                eprintln!("Could not cleanup previous state! Ignoring...");
            }

            if let WallpaperMode::Spanned = this.global.mode() {
                // Can only create a spanned wallpaper if there are outputs available to span
                // across. Specifying outputs on the cmdline means they aren't available since they
                // have their own parameters to apply that would be incompatable with the way that
                // spanned wallpapers are setup.
                if !unoverridden_outputs.is_empty() {
                    let completed = setup_spanned(
                        &mut this.global,
                        unoverridden_outputs.as_slice(),
                        &env_table,
                        lua,
                    )?;
                    for (output_name, wallpaper_location) in completed {
                        exports.set(output_name, wallpaper_location)?;
                    }
                }

                // The case when global is spanned and there are overrides.
                // If all available_outputs have no override, then they are just part of the
                // spanned wallpaper.
                if available_outputs.len() > unoverridden_outputs.len() {
                    // Ensure that all overrides have a source_path.
                    this.update_override_fallbacks()?;
                    for (output, wallpaper_override) in &mut this.overrides {
                        let locations =
                            setup_simple(wallpaper_override, output.name(), &env_table, lua)?;

                        let wallpaper_path: String = locations.get(LOCATION_TABLE_WALLPAPER_KEY)?;
                        let wallpaper_cache_path: String =
                            locations.get(LOCATION_TABLE_WALLPAPER_CACHE_KEY)?;

                        let lockscreen_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_KEY)?;
                        let lockscreen_cache_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_CACHE_KEY)?;

                        wallpaper_override
                            .add_state_keyedpath(wallpaper_path, String::from(output.name()));

                        wallpaper_override
                            .add_cache_keyedpath(wallpaper_cache_path, String::from(output.name()));

                        wallpaper_override.add_lockscreen_state_keyedpath(
                            lockscreen_path,
                            String::from(output.name()),
                        );

                        wallpaper_override.add_lockscreen_cache_keyedpath(
                            lockscreen_cache_path,
                            String::from(output.name()),
                        );
                        exports.set(String::from(output.name()), locations)?;
                    }
                }
            }
            // When global is not spanned and there are no overrides, just create a single image +
            // the lockscreen for all outputs to use.
            else if this.overrides.is_empty() {
                let locations = setup_simple(&this.global, "all", &env_table, lua)?;
                let wallpaper_path: String = locations.get(LOCATION_TABLE_WALLPAPER_KEY)?;
                let lockscreen_path: String = locations.get(LOCATION_TABLE_LOCKSCREEN_KEY)?;

                let wallpaper_cache_path: String =
                    locations.get(LOCATION_TABLE_WALLPAPER_CACHE_KEY)?;
                let lockscreen_cache_path: String =
                    locations.get(LOCATION_TABLE_LOCKSCREEN_CACHE_KEY)?;

                for output in &available_outputs {
                    this.global
                        .add_state_keyedpath(&wallpaper_path, String::from(output.name()));

                    this.global
                        .add_cache_keyedpath(&wallpaper_cache_path, String::from(output.name()));

                    this.global.add_lockscreen_state_keyedpath(
                        &lockscreen_path,
                        String::from(output.name()),
                    );

                    this.global.add_lockscreen_cache_keyedpath(
                        &lockscreen_cache_path,
                        String::from(output.name()),
                    );

                    exports.set(String::from(output.name()), locations.clone())?;
                }
            }
            // If global isn't spanned and there are overrides, then make everything and any
            // outputs that are not overridden gets global.
            else {
                // Update StateManager to ensure that all overrides have a source_path.
                this.update_override_fallbacks()?;
                // If all available_outputs are overridden, then there is no need to make global.
                if num_overridden == available_outputs.len() {
                    for (output, wallpaper) in &mut this.overrides {
                        let locations = setup_simple(wallpaper, output.name(), &env_table, lua)?;

                        let wallpaper_path: String = locations.get(LOCATION_TABLE_WALLPAPER_KEY)?;
                        let lockscreen_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_KEY)?;

                        let wallpaper_cache_path: String =
                            locations.get(LOCATION_TABLE_WALLPAPER_CACHE_KEY)?;
                        let lockscreen_cache_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_CACHE_KEY)?;

                        wallpaper.add_state_keyedpath(wallpaper_path, String::from(output.name()));
                        wallpaper
                            .add_cache_keyedpath(wallpaper_cache_path, String::from(output.name()));

                        wallpaper.add_lockscreen_state_keyedpath(
                            lockscreen_path,
                            String::from(output.name()),
                        );

                        wallpaper.add_lockscreen_state_keyedpath(
                            lockscreen_cache_path,
                            String::from(output.name()),
                        );
                        exports.set(String::from(output.name()), locations)?;
                    }
                } else {
                    let global_locations = setup_simple(&this.global, "default", &env_table, lua)?;
                    // Output that would need to use global are the same that would be available
                    // for spanning.
                    for output in unoverridden_outputs {
                        let wallpaper_path: String =
                            global_locations.get(LOCATION_TABLE_WALLPAPER_KEY)?;
                        let lockscreen_path: String =
                            global_locations.get(LOCATION_TABLE_LOCKSCREEN_KEY)?;

                        let wallpaper_cache_path: String =
                            global_locations.get(LOCATION_TABLE_WALLPAPER_CACHE_KEY)?;
                        let lockscreen_cache_path: String =
                            global_locations.get(LOCATION_TABLE_LOCKSCREEN_CACHE_KEY)?;

                        this.global
                            .add_state_keyedpath(wallpaper_path, String::from(output.name()));
                        this.global
                            .add_cache_keyedpath(wallpaper_cache_path, String::from(output.name()));

                        this.global.add_lockscreen_state_keyedpath(
                            lockscreen_path,
                            String::from(output.name()),
                        );
                        this.global.add_lockscreen_cache_keyedpath(
                            lockscreen_cache_path,
                            String::from(output.name()),
                        );

                        exports.set(String::from(output.name()), global_locations.clone())?;
                    }

                    for (output, wallpaper) in &mut this.overrides {
                        let locations = setup_simple(wallpaper, output.name(), &env_table, lua)?;

                        let wallpaper_path: String = locations.get(LOCATION_TABLE_WALLPAPER_KEY)?;
                        let lockscreen_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_KEY)?;

                        let wallpaper_cache_path: String =
                            locations.get(LOCATION_TABLE_WALLPAPER_CACHE_KEY)?;
                        let lockscreen_cache_path: String =
                            locations.get(LOCATION_TABLE_LOCKSCREEN_CACHE_KEY)?;

                        wallpaper.add_state_keyedpath(wallpaper_path, String::from(output.name()));
                        wallpaper
                            .add_cache_keyedpath(wallpaper_cache_path, String::from(output.name()));

                        wallpaper.add_lockscreen_state_keyedpath(
                            lockscreen_path,
                            String::from(output.name()),
                        );
                        wallpaper.add_lockscreen_cache_keyedpath(
                            lockscreen_cache_path,
                            String::from(output.name()),
                        );

                        exports.set(String::from(output.name()), locations)?;
                    }
                }
            }

            let serialized: Vec<u8> = to_allocvec(this).map_err(Error::external)?;
            let mut current_state_path = PathBuf::new();
            current_state_path.push(state_dir);
            current_state_path.push(CURRENT_STATEMANAGER_FILENAME);

            let current_manager_hash = this.base64_hash();

            let mut current_cache_path = PathBuf::new();
            current_cache_path.push(&cache_dir);
            current_cache_path.push(&current_manager_hash);
            current_cache_path.set_extension("bin");

            let mut cache_index_path = PathBuf::new();
            cache_index_path.push(cache_dir);
            cache_index_path.push(CACHED_STATEMANAGER_INDEX_FILENAME);
            let mut cache_index: File = if cache_index_path.is_file() {
                File::options().append(true).open(cache_index_path)?
            } else {
                File::create(cache_index_path)?
            };

            cache_index.write_all(format!("{}\n", current_manager_hash).as_bytes())?;

            File::create(current_state_path)?.write_all(&serialized)?;
            File::create(current_cache_path)?.write_all(&serialized)?;

            Ok(exports)
        });

        methods.add_method_mut("restore_cache", |lua, this, env_table: Table| {
            let exports = lua.create_table()?;

            // Since there is no guarantee about the ordering of KeyedPaths in Wallpaper, it's best
            // to manually keep track of matching cache_paths to state_paths. Wallpaper paths and
            // lockscreen paths must also be kept separated otherwise there would be no way to tell
            // if a cached_path is a lockscreen or not. State paths at least have the '-lockscreen'
            // suffix, but even that can't be used since Utsukumi should work regardless of the
            // paths themselves, and instead just keep track of them.
            let mut wallpaper_cache_paths: Vec<KeyedPath> = Vec::new();
            let mut lockscreen_cache_paths: Vec<KeyedPath> = Vec::new();

            let wallpaper_state_paths: HashMap<String, PathBuf> = this
                .overrides
                .values_mut()
                .flat_map(|o| o.paths_drain(..))
                .chain(this.global.paths_drain(..))
                .filter_map(|keyed_path| {
                    if let KeyedPath::State(output_name, path) = keyed_path {
                        Some((output_name, path))
                    } else {
                        wallpaper_cache_paths.push(keyed_path);
                        None
                    }
                })
                .collect();

            let lockscreen_state_paths: HashMap<String, PathBuf> = this
                .overrides
                .values_mut()
                .flat_map(|o| o.lockscreen_paths_drain(..))
                .chain(this.global.lockscreen_paths_drain(..))
                .filter_map(|keyed_path| {
                    if let KeyedPath::State(output_name, path) = keyed_path {
                        Some((output_name, path))
                    } else {
                        lockscreen_cache_paths.push(keyed_path);
                        None
                    }
                })
                .collect();

            // Now that all the State paths have been found, we should just be able to find matches
            // by just using the output_name in the relevant hashmap.
            for keyed_path in wallpaper_cache_paths {
                if let KeyedPath::Cache(output_name, path) = keyed_path {
                    let paths = lua.create_table()?;

                    let state_path = wallpaper_state_paths
                        .get(&output_name)
                        .expect("output has cached path but no state path!");

                    paths.set("state_path", state_path.to_string_lossy())?;
                    paths.set("cache_path", path.to_string_lossy())?;

                    exports.set(output_name, paths)?;
                }
            }

            for keyed_path in lockscreen_cache_paths {
                if let KeyedPath::Cache(output_name, path) = keyed_path {
                    let paths = lua.create_table()?;

                    let state_path = lockscreen_state_paths
                        .get(&output_name)
                        .expect("output has cached path but no state path!");

                    paths.set("state_path", state_path.to_string_lossy())?;
                    paths.set("cache_path", path.to_string_lossy())?;

                    let table_key = format!("{output_name}-lockscreen");

                    exports.set(table_key, paths)?;
                }
            }

            // Allow lua to copy the cached StateManager to state_dir.
            let cache_dir: String = env_table.get(ENV_TABLE_CACHE_DIR_KEY)?;
            let mut cached_state_manager_path = PathBuf::new();
            cached_state_manager_path.push(cache_dir);
            cached_state_manager_path.push(this.base64_hash());
            cached_state_manager_path.set_extension("bin");

            exports.set("manager", cached_state_manager_path.to_string_lossy())?;

            // Cleanup current state if it exists.
            let state_dir: String = env_table.get(ENV_TABLE_STATE_DIR_KEY)?;
            let mut current_state = PathBuf::new();
            current_state.push(state_dir);
            current_state.push(CURRENT_STATEMANAGER_FILENAME);

            if current_state.is_file() {
                if let Ok(manager) = StateManager::from_cache(current_state) {
                    manager.cleanup_state();
                } else {
                    eprintln!("Could not cleanup previous state! Ignoring...");
                }
            }

            Ok(exports)
        });

        methods.add_method_mut("login", |lua, this, ()| {
            let exports = lua.create_table()?;

            // Instead of trying to determine which state is being used and then only cheking the
            // relevant fields (e.g. overrides being empty, so only global is checked), just try to
            // get all the possible paths that StateManager could hold and only push
            // KeyedPath::State to the export table.
            let paths = this
                .overrides
                .values_mut()
                .flat_map(|o| o.paths_drain(..))
                .chain(this.global.paths_drain(..));

            for keyed_path in paths {
                if let KeyedPath::State(output, path) = keyed_path {
                    exports.set(output, path.to_string_lossy())?;
                }
            }

            Ok(exports)
        });

        methods.add_method_mut("lockscreen", |lua, this, ()| {
            let exports = lua.create_table()?;

            let paths = this
                .overrides
                .values_mut()
                .flat_map(|o| o.lockscreen_paths_drain(..))
                .chain(this.global.lockscreen_paths_drain(..));

            for keyed_path in paths {
                if let KeyedPath::State(output, lockscreen) = keyed_path {
                    exports.set(output, lockscreen.to_string_lossy())?;
                }
            }

            Ok(exports)
        });
    }
}
