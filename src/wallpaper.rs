use base64ct::{Base64Url, Encoding};
use blake2::{Blake2b512, Digest};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fs::File;
use std::path::{Path, PathBuf};

#[derive(Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum KeyedPath {
    State(String, PathBuf),
    Cache(String, PathBuf),
}

#[derive(PartialEq, Eq, Hash, Debug, Serialize, Deserialize)]
pub struct Wallpaper {
    paths: Vec<KeyedPath>,
    source_path: PathBuf,
    lockscreen_paths: Vec<KeyedPath>,
    offset: (i32, i32),
    rotation_degrees: u16,
    source_crop: String,
    mode: WallpaperMode,
    video_path: Option<PathBuf>,
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy, Serialize, Deserialize)]
pub enum WallpaperMode {
    SimpleFill,
    Spanned,
}

impl Wallpaper {
    pub fn new() -> Self {
        Wallpaper {
            paths: Vec::new(),
            source_path: PathBuf::new(),
            lockscreen_paths: Vec::new(),
            offset: (0, 0),
            rotation_degrees: 0,
            source_crop: String::new(),
            mode: WallpaperMode::SimpleFill,
            video_path: None,
        }
    }

    pub fn paths(&self) -> &[KeyedPath] {
        self.paths.as_slice()
    }

    pub fn paths_drain<R>(&mut self, range: R) -> std::vec::Drain<'_, KeyedPath>
    where
        R: std::ops::RangeBounds<usize>,
    {
        self.paths.drain(range)
    }

    pub fn source_path(&self) -> &Path {
        self.source_path.as_path()
    }

    pub fn set_source_path<T: AsRef<Path>>(&mut self, path: T) {
        self.source_path = path.as_ref().to_path_buf();
    }

    pub fn lockscreen_paths(&self) -> &[KeyedPath] {
        self.lockscreen_paths.as_slice()
    }

    pub fn lockscreen_paths_drain<R>(&mut self, range: R) -> std::vec::Drain<'_, KeyedPath>
    where
        R: std::ops::RangeBounds<usize>,
    {
        self.lockscreen_paths.drain(range)
    }

    pub fn add_state_keyedpath<T: AsRef<Path>>(&mut self, path: T, key: String) {
        self.paths
            .push(KeyedPath::State(key, path.as_ref().to_path_buf()));
    }

    pub fn add_cache_keyedpath<T: AsRef<Path>>(&mut self, path: T, key: String) {
        self.paths
            .push(KeyedPath::Cache(key, path.as_ref().to_path_buf()));
    }

    pub fn add_lockscreen_state_keyedpath<T: AsRef<Path>>(&mut self, path: T, key: String) {
        self.lockscreen_paths
            .push(KeyedPath::State(key, path.as_ref().to_path_buf()));
    }

    pub fn add_lockscreen_cache_keyedpath<T: AsRef<Path>>(&mut self, path: T, key: String) {
        self.lockscreen_paths
            .push(KeyedPath::Cache(key, path.as_ref().to_path_buf()));
    }

    pub fn offset(&self) -> &(i32, i32) {
        &self.offset
    }

    pub fn rotation_degrees(&self) -> &u16 {
        &self.rotation_degrees
    }

    pub fn source_crop(&self) -> &str {
        &self.source_crop
    }

    pub fn mode(&self) -> &WallpaperMode {
        &self.mode
    }

    pub fn video_path(&self) -> &Option<PathBuf> {
        &self.video_path
    }

    pub fn hash(&self) -> [u8; 64] {
        let mut hasher = Blake2b512::new();

        // Use image data instead of the path itself so that the hash isn't dependent on the image
        // location, just the image itself. Instead of returning an error, just panic since this
        // should only be called after the image is already loaded so the path should exist. There
        // also isn't a real way to recover without just choosing to not cache. So since the result
        // would also just lead to the overall script exiting, then I might as well get a better
        // error message than a generic io error.
        let mut source_file =
            File::open(self.source_path()).expect("could not open source to hash!");

        let _bytes = std::io::copy(&mut source_file, &mut hasher);

        hasher.update(self.offset.0.to_le_bytes());
        hasher.update(self.offset.1.to_le_bytes());
        hasher.update(self.source_crop.as_bytes());
        hasher.update(self.rotation_degrees.to_le_bytes());
        hasher.update((self.mode as usize).to_le_bytes());

        hasher.finalize().into()
    }

    pub fn base64_hash(&self) -> String {
        Base64Url::encode_string(&self.hash())
    }
}

impl Default for Wallpaper {
    fn default() -> Self {
        Self::new()
    }
}

impl fmt::Display for Wallpaper {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::from("=== Wallpaper ===\n");

        let source_line = format!("Source: {}\n", &self.source_path.display());
        output.push_str(&source_line);

        if let Some(video_path) = &self.video_path {
            let source_video_line = format!("Video: {}\n", video_path.display());
            output.push_str(&source_video_line);
        }

        let crop = if self.source_crop.is_empty() {
            "None"
        } else {
            &self.source_crop
        };

        let args_line = format!(
            "\n== Modifiers ==\nOffset: {} {}\nRotation: {}\nCrop: {}\nMode: {:?}\n",
            self.offset.0, self.offset.1, self.rotation_degrees, crop, self.mode,
        );
        output.push_str(&args_line);

        let cached_paths = {
            let mut paths = String::new();

            for keyed_path in &self.lockscreen_paths {
                if let KeyedPath::Cache(output, path) = keyed_path {
                    paths.push_str(&format!("{}: {}\n", output, path.display()))
                }
            }

            for keyed_path in &self.paths {
                if let KeyedPath::Cache(output, path) = keyed_path {
                    paths.push_str(&format!("{}: {}\n", output, path.display()))
                }
            }

            if paths.is_empty() {
                String::from("No cached paths.")
            } else {
                paths
            }
        };

        let state_paths = {
            let mut paths = String::new();

            for keyed_path in &self.paths {
                if let KeyedPath::State(output, path) = keyed_path {
                    paths.push_str(&format!("{}: {}\n", output, path.display()))
                }
            }

            for keyed_path in &self.lockscreen_paths {
                if let KeyedPath::State(output, path) = keyed_path {
                    paths.push_str(&format!("{}: {}\n", output, path.display()))
                }
            }

            if paths.is_empty() {
                String::from("No state paths.")
            } else {
                paths
            }
        };

        let paths_line = format!("\n== State ==\n{state_paths}\n== Cached ==\n{cached_paths}");
        output.push_str(&paths_line);

        write!(f, "{}", output)
    }
}

#[derive(Default)]
pub struct WallpaperBuilder {
    paths: Option<Vec<KeyedPath>>,
    source_path: Option<PathBuf>,
    lockscreen_paths: Option<Vec<KeyedPath>>,
    offset: Option<(i32, i32)>,
    rotation_degrees: Option<u16>,
    source_crop: Option<String>,
    mode: Option<WallpaperMode>,
    video_path: Option<PathBuf>,
}

impl WallpaperBuilder {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn source_path<T: AsRef<Path>>(&mut self, path: T) -> Result<&mut Self, std::io::Error> {
        let path = path.as_ref().as_os_str().to_string_lossy();
        let expanded_path = shellexpand::full(&path)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidInput, e))?;
        self.source_path = Some(std::fs::canonicalize(expanded_path.to_string())?);
        Ok(self)
    }

    pub fn offset(&mut self, offset: (i32, i32)) -> &mut Self {
        self.offset = Some(offset);
        self
    }

    pub fn rotation_degrees(&mut self, rotation_degrees: u16) -> &mut Self {
        self.rotation_degrees = Some(rotation_degrees);
        self
    }

    pub fn source_crop(&mut self, source_crop: String) -> &mut Self {
        self.source_crop = Some(source_crop);
        self
    }

    pub fn mode(&mut self, mode: WallpaperMode) -> &mut Self {
        self.mode = Some(mode);
        self
    }

    pub fn video_path<T: AsRef<Path>>(&mut self, path: T) -> Result<&mut Self, std::io::Error> {
        let path = path.as_ref().as_os_str().to_string_lossy();
        let expanded_path = shellexpand::full(&path)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidInput, e))?;

        self.video_path = Some(std::fs::canonicalize(expanded_path.to_string())?);
        Ok(self)
    }

    pub fn build(self) -> Wallpaper {
        let paths = {
            if let Some(paths) = self.paths {
                paths
            } else {
                Vec::new()
            }
        };
        let source_path = {
            if let Some(source_path) = self.source_path {
                source_path
            } else {
                PathBuf::new()
            }
        };
        let lockscreen_paths = {
            if let Some(lockscreen_paths) = self.lockscreen_paths {
                lockscreen_paths
            } else {
                Vec::new()
            }
        };
        let offset = {
            if let Some(offset) = self.offset {
                offset
            } else {
                (0, 0)
            }
        };
        let rotation_degrees = {
            if let Some(rotation_degrees) = self.rotation_degrees {
                rotation_degrees
            } else {
                0
            }
        };
        let source_crop = {
            if let Some(source_crop) = self.source_crop {
                source_crop
            } else {
                String::new()
            }
        };
        let mode = {
            if let Some(mode) = self.mode {
                mode
            } else {
                WallpaperMode::SimpleFill
            }
        };
        let video_path = self.video_path;

        Wallpaper {
            paths,
            source_path,
            lockscreen_paths,
            offset,
            rotation_degrees,
            source_crop,
            mode,
            video_path,
        }
    }
}
