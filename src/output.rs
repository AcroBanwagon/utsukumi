use mlua::Table;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt::Display;
use std::str::FromStr;

#[derive(PartialEq, Eq, Hash, Debug, Serialize, Deserialize)]
pub struct Position(pub u32, pub u32);
#[derive(PartialEq, Eq, Hash, Debug, Serialize, Deserialize)]
pub struct Resolution(pub u32, pub u32);

#[derive(Debug)]
pub struct ParseOutputTransformError;

#[derive(PartialEq, Eq, Hash, Debug, Serialize, Deserialize)]
pub enum OutputTransform {
    Normal,
    Rotated90,
    Rotated180,
    Rotated270,
}

impl FromStr for OutputTransform {
    type Err = ParseOutputTransformError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "270" {
            Ok(OutputTransform::Normal)
        } else if s == "180" {
            Ok(OutputTransform::Rotated180)
        } else if s == "90" {
            Ok(OutputTransform::Rotated90)
        } else if s == "normal" {
            Ok(OutputTransform::Normal)
        } else {
            Err(ParseOutputTransformError)
        }
    }
}

impl Display for ParseOutputTransformError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "string is not a valid output transformation")
    }
}

impl Error for ParseOutputTransformError {}

impl From<ParseOutputTransformError> for mlua::Error {
    fn from(value: ParseOutputTransformError) -> Self {
        mlua::Error::external(value)
    }
}

#[derive(Hash, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Output {
    name: String,
    resolution: Resolution,
    position: Position,
    transform: OutputTransform,
}

impl Output {
    pub fn new() -> Self {
        Output {
            name: String::new(),
            resolution: Resolution(0, 0),
            position: Position(0, 0),
            transform: OutputTransform::Normal,
        }
    }

    pub fn from_table(params: &Table) -> mlua::Result<Self> {
        let mut builder = OutputBuilder::new();

        builder.name(params.get("name")?);
        builder.position(Position(params.get("pos_x")?, params.get("pos_y")?));
        builder.resolution(Resolution(params.get("width")?, params.get("height")?));

        let transform: String = params.get("transform")?;
        builder.transform(OutputTransform::from_str(&transform)?);

        Ok(builder.build())
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn position(&self) -> &Position {
        &self.position
    }

    pub fn resolution(&self) -> &Resolution {
        &self.resolution
    }

    pub fn transform(&self) -> &OutputTransform {
        &self.transform
    }
}

impl Default for Output {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Default)]
pub struct OutputBuilder {
    name: Option<String>,
    resolution: Option<Resolution>,
    position: Option<Position>,
    transform: Option<OutputTransform>,
}

impl OutputBuilder {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn name(&mut self, name: String) -> &mut Self {
        self.name = Some(name);
        self
    }

    pub fn resolution(&mut self, res: Resolution) -> &mut Self {
        self.resolution = Some(res);
        self
    }

    pub fn position(&mut self, pos: Position) -> &mut Self {
        self.position = Some(pos);
        self
    }

    pub fn transform(&mut self, transform: OutputTransform) -> &mut Self {
        self.transform = Some(transform);
        self
    }

    pub fn build(self) -> Output {
        let name = {
            if self.name.is_some() {
                self.name.unwrap()
            } else {
                String::new()
            }
        };
        let resolution = {
            if self.resolution.is_some() {
                self.resolution.unwrap()
            } else {
                Resolution(0, 0)
            }
        };
        let position = {
            if self.position.is_some() {
                self.position.unwrap()
            } else {
                Position(0, 0)
            }
        };
        let transform = {
            if self.transform.is_some() {
                self.transform.unwrap()
            } else {
                OutputTransform::Normal
            }
        };

        Output {
            name,
            resolution,
            position,
            transform,
        }
    }
}
