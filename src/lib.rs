pub(crate) mod cli;
pub(crate) mod consts;
pub(crate) mod output;
pub(crate) mod state;
pub(crate) mod utils;
pub(crate) mod wallpaper;

use crate::consts::*;
use clap::Parser;
use cli::Cli;
use mlua::prelude::*;
use state::StateManager;
use std::path::PathBuf;

#[mlua::lua_module]
fn libutsukumi(lua: &Lua) -> LuaResult<LuaTable> {
    let mod_table = lua.create_table()?;
    mod_table.set("state_from_args", lua.create_function(state_from_args)?)?;
    Ok(mod_table)
}

fn state_from_args<'a>(
    lua: &'a Lua,
    (cmdline, env_table): (LuaTable, LuaTable),
) -> LuaResult<LuaTable<'a>> {
    let parsed_table = lua.create_table()?;
    let mut args: Vec<String> = Vec::new();
    let mut from_current_cache = false;
    for arg in cmdline.sequence_values() {
        args.push(String::from_lua(arg?, lua)?);
    }

    // Clap can't natively handle a usecase where options can be used multiple times for different
    // objects. (clap-rs issue #1704) In order to do that, I need to split the cmdline into each
    // group and then parse them separately. Clap expects to have the binary name as the first
    // argument otherwise it errors.
    let split_args: Vec<Vec<String>> = args
        .split(|arg| arg == "-o" || arg == "--output")
        .map(|slice| {
            let mut vec = slice.to_vec();
            if let Some(output_name) = vec.get(0) {
                // The expected format is either the binary name followed by args, or an output
                // name followed by args since split() removes the -o arg. However, this misses the
                // case where it is only args. I could try to catch that, but in the (unlikely)
                // case that an output name also starts with a '-' then that would go through the
                // incorrect branch. IMO, the only reliable way to avoid that would be to match the
                // entire string against the known arguments and if it doesn't treat it as an
                // output name. However, matching args is kinda the whole point of clap and this is
                // only to sanitize input for clap. The lua frontend just needs to ensure that the
                // binary name is always the first argument. There isn't really a need to catch
                // that case anyway since clap should error.
                if output_name == PROJECT_NAME {
                    // Any arguments before the first "-o" arg are for the global output space. The
                    // binary name should always be here at least. insert pushes everything else in the
                    // Vec down so I can ensure that the order is correct by inserting them in the
                    // reverse order they should appear. This should result in something that look like
                    // this: [ PROJECT_NAME, "-o", "Global", .. ]
                    vec.insert(1, String::from("Global"));
                    vec.insert(1, String::from("-o"));
                } else {
                    vec.insert(0, String::from("-o"));
                    vec.insert(0, String::from(PROJECT_NAME));
                }
            }
            vec
        })
        .filter(|vec| vec.len() > 3)
        .collect();

    let mut parsed_args: Vec<Cli> = Vec::new();
    for arg_group in split_args {
        // Instead of letting lua handle the error, just let clap handle it and print out the nice
        // included messages it provides, as well as not returning a lua error when --help is
        // passed.
        let parsed = cli::Cli::parse_from(arg_group);

        // Forward arguments to lua. These are arguments that aren't needed to build StateManager,
        // and also might be used to determine which cached StateManager should be used. Lua must
        // determine the code path to take by either choosing to build a brand new StateManager
        // (i.e. not passing --login/--lockscreen) or getting one from cache. There are two options
        // for cache, either the current manager in the state_dir, or build StateManager from args
        // and then check if it exists in cache_dir to avoid expensive image operations.
        //
        // Return early to avoid hashing StateManager::default(). When utsukumi is called with only
        // --emergency as an arg, the returned StateManager is built from_args and contains no
        // paths. The point of emergency is to enable special handling, which is why it's set in
        // the exported table. Without having a special emergency StateManager, it's better to not
        // return anything than to return an empty and unused manager.
        if let Some(cached) = parsed.dump_cached {
            let manager = StateManager::from_cache(cached).unwrap_or_else(|e| {
                eprintln!("Invalid or corrupted StateManager cache!");
                eprintln!("{e}");
                std::process::exit(1);
            });
            println!("{}", manager);
            std::process::exit(0);
        }
        if parsed.is_login {
            parsed_table.set("login", true)?;
            from_current_cache = true;
        }
        if parsed.is_lockscreen {
            parsed_table.set("lockscreen", true)?;
            from_current_cache = true;
        }
        if parsed.is_emergency {
            parsed_table.set("emergency", true)?;
            return Ok(parsed_table);
        }
        parsed_args.push(parsed);
    }

    let mut manager = if from_current_cache {
        let state_dir: String = env_table.get(ENV_TABLE_STATE_DIR_KEY)?;
        let mut cached = PathBuf::new();
        cached.push(state_dir);
        cached.push(CURRENT_STATEMANAGER_FILENAME);
        StateManager::from_cache(cached).unwrap_or_else(|e| {
            eprintln!("Invalid or corrupted StateManager cache!");
            eprintln!("{e}");
            std::process::exit(1);
        })
    } else {
        let outputs: LuaTable = env_table.get(ENV_TABLE_OUTPUTS_KEY)?;
        StateManager::from_args(parsed_args, &outputs)?
    };

    // Ensure that all overrides have a source_path set, instead of doing so in each branch that
    // uses overrides.
    manager.update_override_fallbacks()?;

    // Only try to get manager from cache if it's not already in state_dir.
    // Must occur after update_override_fallbacks otherwise the hash could differ.
    let cache_path: String = env_table.get(ENV_TABLE_CACHE_DIR_KEY)?;
    if manager.is_cached_at_path(&cache_path)? && !from_current_cache {
        let mut cached_manager_path = PathBuf::new();
        cached_manager_path.push(cache_path);
        cached_manager_path.push(manager.base64_hash());
        cached_manager_path.set_extension("bin");

        let cached_manager = StateManager::from_cache(cached_manager_path)?;
        parsed_table.set("cached", true)?;
        manager = cached_manager;
    }

    parsed_table.set("manager", manager)?;
    Ok(parsed_table)
}
