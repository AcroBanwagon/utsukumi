pub(crate) const PROJECT_NAME: &str = "utsukumi";
pub(crate) const LOCATION_TABLE_WALLPAPER_KEY: &str = "wallpaper";
pub(crate) const LOCATION_TABLE_WALLPAPER_CACHE_KEY: &str = "wallpaper-cache";
pub(crate) const LOCATION_TABLE_LOCKSCREEN_KEY: &str = "lockscreen";
pub(crate) const LOCATION_TABLE_LOCKSCREEN_CACHE_KEY: &str = "lockscreen-cache";
pub(crate) const LOCATION_TABLE_VIDEO_KEY: &str = "video";
pub(crate) const ENV_TABLE_STATE_DIR_KEY: &str = "state_dir";
pub(crate) const ENV_TABLE_CACHE_DIR_KEY: &str = "cache_dir";
pub(crate) const ENV_TABLE_OUTPUTS_KEY: &str = "outputs";
pub(crate) const CURRENT_STATEMANAGER_FILENAME: &str = "current.bin";
pub(crate) const CACHED_STATEMANAGER_INDEX_FILENAME: &str = "index";
