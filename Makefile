DESTDIR =
prefix ?= /usr
datarootdir ?= $(prefix)/share
datadir = $(datarootdir)
libdir = $(prefix)/lib
INSTALL = install
INSTALL_DATA = $(INSTALL) -Dm 644
LUA_CMODULE_DIR ?= $(libdir)/lua/5.4

default:
	@echo make target unspecified, doing nothing...

build-release:
	cargo build --release

install:
	$(INSTALL_DATA) target/release/libutsukumi.so "$(DESTDIR)$(LUA_CMODULE_DIR)/libutsukumi.so"
	$(INSTALL_DATA) target/release/build/utsukumi-*/out/_utsukumi "$(DESTDIR)$(datadir)/zsh/site-functions/_utsukumi"
	$(INSTALL_DATA) target/release/build/utsukumi-*/out/utsukumi.bash "$(DESTDIR)$(datadir)/bash-completion/completions/utsukumi.bash"
	$(INSTALL_DATA)	target/release/build/utsukumi-*/out/utsukumi.fish "$(DESTDIR)$(datadir)/fish/vendor_completions.d/utsukumi.fish"
